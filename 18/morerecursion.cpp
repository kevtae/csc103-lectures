#include<iostream>
using std::cin;
using std::cout;
using std::endl;

/* TODO: write the *extended* GCD algorithm, which returns gcd(a,b), but
 * also sets x and y such that ax + by = gcd(a,b).  We did this in lecture,
 * but try to do it here from scratch (don't look at the notes unless you
 * have to). */


int xgcd(int a, int b, int& u, int& v)
{
	if(b==0){
		u = 1;
		v=0;
		return a;
	}
	int x1, y1;
	int d = xgcd(b, a%b, x1, y1);
	u= y1;
	v= x1-(a/b)*y1;
	return d;
}

int main(int argc, char** argv)
{
	int u=1;
	int v=2;
	int x= xgcd(18,12,u,v);
	cout<< x << endl;
	cout<< v<< endl;
	cout << u << endl;
	return 0;
}