/* strings.cpp
 * More examples working with strings...
 * */
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;

string teststring = "hikevin";
string teststring1 = "kev";

/* TODO:  write a function that takes a string and a character
 * and returns the number of occurences of the character in the string.
 * */




size_t countChars(const string& s, char l)
{
	/* TODO: remove the const modifier and try to run this function
	 * on teststring above.  Take note of the compiler error in case
	 * you see it again later (the message from g++ might not be all
	 * that easy to read/understand). */
int x=0;
for(size_t i=0; i <= s.length()-1; i++){
 if(s[i] == 'l'){
  x ++;
 }
}
 cout << x << endl;
}

/* TODO: write the following function which converts all lower
 * case letters to upper case.  NOTE: there are library functions
 * that will convert a single character (see 'man 3 toupper'),
 * but it might be more instructive to do it without using those.
 * If doing it without the help of toupper, note that the offset
 * between lower/upper cases is simply 'A' - 'a' (remember, this
 * is a number...).  See the ascii-test.cpp as well.
 * */
void YELL(string& s) {
	/* make all lower case chars upper case. */
 for(size_t i=0; i <= s.length()-1; i++){
 if ((s[i] > 96) && (s[i] < 123)){
  s[i] = s[i] - 32;
  }
 }
}

/* TODO: write a function that takes a string and returns a boolean
 * indicating whether or not it was a palindrome.
 * */
bool palindrome(const string&s){
 for(size_t i=0; i < s.length(); i++){
  if(s[i] != s[s.length()-i-1]){
   return false;
   break;
  }
  else return true;
}
}

/* TODO: write a function that takes a string by reference and reverses it.
 * */

void reverse(string &s){
 char temp;
 for(size_t i=0;i < (s.length()/2);i++){
  temp = s[i];
  s[i] = s[s.length()-i-1];
  s[s.length()-i-1] = temp;
 }
}

/* TODO: write a function that takes two strings and returns an integer value
 * indicating whether or not the first was a substring of the second.  Remember
 * that the return value should be the index at which the string was found, or
 * -1 to indicate that the string was not found.  NOTE: this might be a little
 * tough.  Maybe do it last.
 * */
int isSubstring(const string& s1, const string& s2) {
  size_t l1=s1.length();
  size_t l2=s2.length();

  for(size_t i = 0; i <= l1-l2; i++){
   size_t j;
   for(j=0; j < l2; j++){
    if(s2[j] != s1[i+j])
     break;
   }
   if(j==l2) return i;
  }
	return -1;
}

/* NOTE: there is a built-in string function for this (find(str,pos)).
 * TODO: try it out (but don't use it to solve the above, of course!).
 * */

int main() {
	/* TODO: write test code for all of the functions you wrote above */
 char l;
 /*countChars(teststring,l);
 YELL(teststring);
 cout << teststring << endl;
 cout << palindrome(teststring)<<endl;
 reverse(teststring);
 cout << teststring << endl;
 */
 int i = isSubstring(teststring,teststring1);
 if (i >= 0) {
		cout << "match found at " << i << "\n";
	} else {
		cout << "no match found.\n";
	}
 return 0;
}
