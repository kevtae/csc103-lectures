#include<iostream>
using std::cin;
using std::cout;
using std::endl;
#include <cstdio>

/* TODO: write the recursive GCD function from lecture and make sure you
 * understand how it works.  In fact, make sure you can *prove* it works.
 * (Keep mind how we defined the size of the input as the size of the
 * second parameter.)
 * */
int gcd(int a, int b)
{
	if(b==0)
	gcd(b,a%b);
}

/* TODO: write the *extended* GCD algorithm, which returns gcd(a,b), but
 * also sets u and v such that ua + vb = gcd(a,b) Warning: this might take a
 * little bit of thinking (if you don't just look up the answer online).  Save
 * it for last. */
int xgcd(int a, int b, int& u, int& v)
{
	if(b==0){
		u = 1;
		v=0;
		return a;
	}
	int x1, y1;
	int d = xgcd(b, a%b, x1, y1);
	u= y1;
	v= x1-(a/b)*y1;
}

void gcdTest()
{
	int a,b,d;
	cout << "Enter pairs of integers a,b for gcd test: ";
	while(cin >> a >> b) {
		d = gcd(a,b);
		printf("gcd(%i,%i) = %i\n",a,b,d);
	}
}

int main()
{
	xgcd(20,30, int u, int v);
	return 0;
}

/* TODO: if you haven't already, write merge sort from last time. */
