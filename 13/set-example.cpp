#include <iostream>
using std::cout;
using std::cin;
#include <set>
using std::set;
#include <vector>
using std::vector;



int main()
{

	set<int> s1 = {2,3,4,5,6,7};
	set<int> s2 = {1,2,4,7,11,44};
	set<int> intersect(const set<int>& S1, const set<int>& S2);
	set<int> unio(const set<int>& S1, const set<int>& S2);
	set<int> s3 = intersect(s1,s2);
	set<int> s4= unio(s1,s2);

	cout << "intersection contained: ";
	for (set<int>::iterator i = s3.begin(); i != s3.end(); i++) {
		cout << *i << "";
	}

	cout <<"\n";


	cout << "union is: ";
	for (set<int>::iterator i = s4.begin(); i != s4.end(); i++) {
		cout << *i << " ";
	}

	cout << "\n";

	vector<int>v;
	v.push_back(1);
	v.push_back(3);
	v.push_back(6);
	v.push_back(9);
	int n;
	cin >> n;

	remove(v,n);

	for(int i =0; i < v.size(); i++){
		cout<< v[i] ;
	}
	return 0;
}

/* TODO: you can use sets to sort a vector or an array.  Try it maybe. */

/* TODO: compute the intersection of two sets.
 * Recall that the intersection of two sets is the set
 * consisting of the elements they have in common.  E.g.,
 * intersection of {2,3,4,5,6,7} and {1,2,4,7,11,44} is
 * {2,4,7}.  Here's a start: */
set<int> intersect(const set<int>& S1, const set<int>& S2) {
	set<int> I;
	/* code goes here... */
	for(set<int>::iterator i = S1.begin(); i != S1.end(); i++){
		if(S2.count(*i))
				I.insert(*i);
	}
	return I;
}
/* TODO: write a function that returns the union of two sets */
set<int> unio(const set<int>& S1, const set<int>& S2) {
	set<int> I;
	/* code goes here... */
	for(set<int>::iterator i = S1.begin(); i != S1.end(); i++){
		I.insert(*i);
	}
	for(set<int>::iterator i = S2.begin(); i != S2.end(); i++){
		if(I.count(*i)){

		}
		else{
			I.insert(*i);
		}
	}
	return I;
}
/* TODO: write a function that takes an index i and a vector
 * V and removes the i-th element.  If you don't have to
 * preserve the order, how could you do this with only a
 * constant number of steps?  (Say, using one call to pop_back()?)
 * */

void remove(vector<int>&v,int&i){
	int n = v.size()-1;
	int temp = v[i];
	v[i] = v[n];
	v[n] = temp;
	v.pop_back();
}

/* Some test code for your intersect function: */


/* NOTE: you'll have to prototype these above main() if you want to
 * call them from main(). */
