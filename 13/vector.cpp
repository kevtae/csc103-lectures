#include <iostream>
using std::cout;
using std::cin;
#include <set>
using std::set;
#include <vector>
using std::vector;

void remove(vector<int>&v,unsigned i){
	int n = v.size()-1;
	int temp = v[i];
	v[i] = v[n];
	v[n] = temp;
	v.pop_back();
}

int main(){

vector<int>v;
	v.push_back(1);
	v.push_back(3);
	v.push_back(6);
	v.push_back(9);

  int n;
	cin >> n;

	remove(v,n++);

	for(int i =0; i < v.size(); i++){
		cout<< v[i] ;
	}

  cout << "\n";

	return 0;
}
