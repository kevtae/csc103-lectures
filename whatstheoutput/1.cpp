#include <iostream>
using std::cout;
using std::endl;

/* TODO: predict the output of this program *without* running it!
 * Then compile and run it to check your answers and make sure you
 * understand what is going on if any of your guesses were wrong. */

int main() {
    int x = 10, y = 3;
    double d = 4, e = 2;
    cout << --x << endl; //9
    cout << x-- << endl; //9
    cout << y/x << endl; //0
    y = e; //2
    cout << e<<endl;
    cout << d << endl;
    cout << (x-d)/2 << endl;
    cout << y/x << endl; //0
    cout << x%y << endl; //0
    cout << (x-d)/e << endl; //2
    cout << (10-4.0)/(2.0) << endl;
    return 0;
}
