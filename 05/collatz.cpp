/* More about loops.
 * See lectures 4,5,6 from Prof. Li.,
 * and chapters 3,4 in the book. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;

int main()
{
	/* Collatz conjecture: for all integers n, the following process
	 * will terminate in a finite number of steps:
	 *    if n == 1, stop;
	 *    if n is even, divide by 2;
	 *    if n is odd, multiply by 3 and add 1;
	 *    repeat using this new value of n.
	 * */
	/* TODO: write a program that tests the conjecture for arbitrarily
	 * many integers, provided on standard input.  (Just print the input
	 * and the number of steps it required before terminating.) */

	int n;
	int t = 0;
	int x=0; //maximum intermediate value
	while(cin>>n){
		while(n != 1){
			if(n%2 == 0){
				n = n/2;
				t++;
					if(n>x){
						x=n;
					}
				}
			else if (n%2 !=0){
				n=n*3+1;
				t++;
					if(n>x){
						x=n;
					}
					}
			}
		cout << "number of step " << t<< "\n";
		cout << "maximum intermediate value " << x << "\n";
			t=0;
			n=0;
			x=0;
	}
	return 0;
}

/* TODO: keep track of the maximal intermediate value and print that out
 * along with the result. */

// vim:foldlevel=1
