/* Lucas sequences. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main()
{
	/* Lucas sequences (of the first kind).  For numbers P,Q, set
	 * U_n = P*U_{n-1} - Q*U_{n-2}.
	 * The first two terms are defined as 0 and 1, respectively.
	 * NOTE: this is a generalization of the fibonacci sequence
	 * (fibonacci is the special case of P==1,Q==-1).
	 * */
	/* TODO: finish our fibonacci code from class, and generalize
	 * to lucas sequences (see above). */

	int a=2;
	int b=1;
	int c; // a+b
	int n; //input
	int t=1; // counter
	cin >> n;

	if(n<2){
		cout<<"2"<<endl;
		return 0;
	}

	while(t != n){
		t++;
		c=a+b;
		a=b;
		b=c;
	}

	cout << b << endl;
	return 0;
}

// vim:foldlevel=1
