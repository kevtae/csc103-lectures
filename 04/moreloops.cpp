/* More about loops.
 * See lectures 4,5,6 from Prof. Li.,
 * and chapters 3,4 in the book. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <cstdio>
#include <math.h>

int main()
{
	/* BIG PICTURE IDEAS WITH LOOPS.
	 * 1. "Brute force" -- leverage the speed of the computer to explicitly
	 *    run through an entire space of potential solutions, checking
	 *    each candidate for validity.  This is usually pretty trivial.
	 * 2. Maintaining invariants -- when computing some aggregate function
	 *    of a long list of inputs, initialize a variable to be something
	 *    like "the right answer so far" and then read new inputs one by
	 *    one, *updating your candidate answer* so that after each iteration
	 *    of the loop, it is ALWAYS the "right answer so far".
	 * NOTE: even brute force typically has an invariant hiding somewhere, but
	 * it usually isn't all that interesting.
	 * Let's see some examples: */

	/* exercise: compute the min of integers supplied via stdin. */
	#if 0
	int n; /* store input */
	int min; /* minimum ofwhat we've seen *so far*. */
	cin >> n;
	min = n; /* NOTE: at this point, min's value is
				consistent with the meaning we gave it! */
	while (cin >> n) {
		if (n < min) min = n;
		/* NOTE: the *invariant* for this loop is that
		 * min is the smallest value we've read so far.
		 * Initialize min to have this property before the
		 * loop, and then at the end of each iteration, make
		 * sure this property remains true (that is to say,
		 * it's *invariant*!!).
		 * */
	}
	cout << min << "\n";
	return 0;
	#endif

	/* TODO: if you haven't already, be sure you can do this on your own,
	 * as well as similar things like the min, the sum, the product, the
	 * average...  Try to take the following very high level steps:
	 * 1. First, think about how many variables you will need, and for what
	 *    purposes.
	 * 2. Declare the variables, and in comments, state the meaning of each
	 *    variable.
	 * 3. Loop through the input and make sure the meaning of each of your
	 *    variables is preserved at the end of each iteration of the loop.
	 * 4. Finally, print the results.
	 * */

	/* exercise: brute force gcd.  Compute the greatest common
	 * divisor of n and k by an exhaustive search.  */
	/* IDEA: start with the largest possible value that the gcd
	 * could have, then walk *backwards* until you find the first
	 * value that divides both inputs. */
	/* Range of possibilities for a valid answer: {min{a,b}...1} */
	# if 0
	{
	int a, b;
	int min; //smaller int of a,b
	cin >> a >> b ;
	if(a < b){
		min = a;
	}
	else {
		min = b;
	}
	int d = min;
	 while( a%d != 0  || b%d != 0){
		 d--;
	 }
	cout << "GCD is " << d << "\n";
	return 0;
}
	#endif

	// Using euclidean algorithm
	# if 1
	{
		int a,b;
		int min;
		int max;
		cin >> a >> b;
		int r;//remainder

		if(a<b){
			min = a;
			max=b;
		}
		else{
			min=b;
			max=a;
		}
	while(max%min != 0){
		r = max%min;
		max = min;
		min =r;
		}


		cout << min << "\n";
		return 0;

	}
	#endif

	/* TODO: brute force test for perfect cubes.  Check if
	 * n = k^3 for some integer k.  */
	 # if 0
	 {
		 int n; //input
		 int k=0; //some integer
		 cin >> n;
		 while(n > k){
			 if (n != pow(k,3)){
			 k++;
			 }
			else if (n == pow(k,3)){
				cout << "input has perfect cube of " << k << "\n";
				return 0;
			}
				 cout<< "not a perfect cube root" << "\n";
				return 0;
	 }
	 #endif


	/* TODO: write a loop that prints the sum of the first n odd cubes. */
	# if 0
	{
		int n; // input
		int x =1; // first odd number
		int a = 0;	// counter
		int z = 0;
		int y;
		cin >> n;

		while( a!= n){
			a++;
			y = x*x*x;
			z = z + y;
			x = x+2;
		}

		cout << "sum of first " << n << " odd cubes is " << z << "\n";
		return 0;
	}
	#endif

	/* TODO: write code that gets an integer n from the user and prints out
	 * the n-th term of the fibonacci sequence. */
	 #if 0
	  { // code gets numbers from fibonnaci sequence and pring out which term it is
		int n; //input
		cin >> n;
		int i = 1;//first
		int a = 1;//second
		int t=2;//counter
		int b=1;// sum of i and a

		 if(n<2){
			 t=2;
		 }

		 while(n != b){
			 t++;
			 i = a;
			 a = b;
			 b= i+a;

		 }
		cout << t << endl;
		 return 0;
	 }
	 #endif

	# if 0
	{ // gets nth term and print out value in fibonacci sequence
	int n;
	cin >> n;
	int a=1;
	int b=1;
	int c;
	int t = 1; // counter

	if(n<2){
		cout<< "1"<< "/n";
		return 0;
	}

	while(t != n){
		t++;
		c=a+b;
		a=b;
		b=c;
	}

	cout << b << "\n";
	return 0;

	}

		#endif

    #if 0
	 {

		 int n;
		 cin >> n;
		 int i = 1;
		 int ai, abefore; // ai = ai, abefore = ai-1
		 ai=1;
		 abefore =1;
		 // note values of ai,abefore, and i are consistent with 	 the meaniningg we gave them!
		 //I.e, the invariant is established

		 if(n < 2) {
			 cout << 1;
			 return 0;
		 }
		 while( i < n){
			 i ++;
			 int anext= abefore + ai;
			 abefore = ai;
			 ai = anext;
		 }
		 // here, we know i==n, and ai= ai=an
		 cout << ai ;
	 }
	#endif


	/* TODO: a slight generalization of an earlier exercise: for integers
	 * n and k, determine the largest power of k that divides n.
	 * NOTE: see if you can formalize the invariant you used to solve this.
	 * */

	 # if 0 // actual version
	 {
	 int n,k;
	 cin >> n >> k;
	 int t=1; // counter

	  if(k>n){
			int a =n;
			n=k;
			k=a;
		}

	 int d= k;

		while(n/k > 1){
			if(n%k == 0){
			k = k*d;
			t++;
			}
			else{
				cout<< n << " is not divisible by" << k << "\n";
				return 0;
			}
	 }

	 cout << d << "^" << t << "\n";
	 return 0;
 }
	 #endif

	 #if 0
	 {
	 int n;		 // inputs
	 int x= 10000000000;
	 int y; //second smallest integer

		 while(cin>>n){
			 if (x>n){
				 y=x;
				 x=n;
			 }
			 else if (y>n){
				 y = n;
			 }
		 }

	 cout << "second smallest integer is " << y << "\n";
 }
	 #endif

	/* TODO: write a program that reads (arbitrarily many) integers from
	 * stdin and outputs the *second* smallest one.  NOTE: you don't need
	 * to store many numbers (all at once, that is) to do this!  You'll
	 * only need a few integer variables.  Think about invariants! */


	/* TODO: this is really basic, but good to be aware of: if you put
	 * the arrows going the wrong way, like "cin << x" or "cout >> x",
	 * you will get some very long, nasty, confusing error messages
	 * from the compiler.  Write code with this mistake on purpose, try to
	 * compile it, and take note of those error messages so you know where
	 * to look if you ever see errors like that again in the future. */

	return 0;
}

// vim:foldlevel=1
