#include <iostream>
using std::endl;
using std::cout;



void swap(int*& a, int*& b)
{
    int* c = a;
    a = b;
    b = c;
}

void reverse(int* A, size_t start, size_t end){
  int temp;
  while(start < end){
    temp = A[start];
    A[start] = A[end-1];
    A[end-1] = temp;
    start++;
    end--;
  }
}

void shift(int* A /* the array */,
		size_t len /* #elements of A */,
		size_t n /* shift amount */){
   reverse(A,0,len);
   reverse(A,0,n);
   reverse(A,n,len);
    }
/* TODO: write a function that performs a "circular shift" on
 * an array of integers.  For example, if the input array
 * contained 0,1,2,3,4 and we shifted by 2, the new array would
 * contain 3,4,0,1,2.
 * NOTE: you can do this with a CONSTANT AMOUNT OF ADDITIONAL STORAGE.
 * That is, irrespective of the size of the array, your function should
 * allocate the same, fixed amount of storage.  No vectors.  No additional
 * arrays.  Just a few integers.  Try it!  (Kind of hard though.)
 * Moreover, do this without repeatedly shifting the array which will
 * take n*len steps.  You should be able to do this in c*len steps
 * for some small constant c, not depending on len.
 * */




int main()
{
    /*int a=10;
    int b=20;
    int* pa = &a;
    int* pb = &b;
cout << "pa == " << pa << "/ta==" << a << endl;
  cout << "pb == " << pb << "/tb==" << b<< endl;
    swap(pa, pb);

  cout << "pa == " << pa << "/ta==" << a << endl;
  cout << "pb == " << pb << "/tb==" << b<< endl;
  */
  const int size = 5;
  int a[size] = {0,1,2,3,4};
  shift(a, 5, 1);
  for(int i=0; i<5; i++)
    cout<<a[i]<< endl;
  return 0;
}