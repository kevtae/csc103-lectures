#include <iostream>
using std::cin;
using std::cout;
#include <string>
using std::string;
using std::endl;
#include <math.h>
#include <vector>
using std::vector;



int sum_from_to(int first, int last){
  int i;
  int x =0;
  for(i =first; i<=last ; i++){
    x = x+i;
  }
  return x;
}

int enough(int goal){
  int n=1, sum =1;

  while(sum < goal)
    sum += ++n;

  return n;
}

int g_c_d(int x, int y){

  if(y>x){
    int temp = x;
    x = y;
    y = temp;
  }
  int z = y;

  while(x%y!= 0 || z%y!=0){
    y--;
  }
  return y;
}

int gcd2(int x, int y){
  if(y>x){
    int temp = x;
    x = y;
    y = temp;
  }
  int r;

  while(x%y != 0){
    r= x%y;
    x= y;
    y = r;
  }
  return r;
}

bool isprime(int x){
  if(x <= 1)
    return 0;
  if(x%2 == 0 && x !=2)
    return false;

  for(int i=3; i <= sqrt(x); i +=2){
    if(x % i == 0)
    return false;
  }
  return true;
}

int reduce(int &x, int &y){

  if(x<=0 || y<=0){
    return 1;
  }

  else{
      int r= gcd2(x,y);
        x=x/r;
        y=y/r;
    }
}

void *swap_floats(float &x, float &y){
  float temp = x;
  x=y;
  y=temp;
}

void sort3(float&a, float&b, float&c){
  if(b>c)
    swap_floats(b,c);
  if(a>c)
    swap_floats(a,c);
  if(a>b)
    swap_floats(a,b);
}

vector<float>V={5.8,2.6,9.1,3.4,7.0,3.4};

void reverse(vector<float>&V){
  for(int i=0;i<=V.size()/2;i++){
    swap_floats(V[i],V[V.size()-i-1]);
  }
}

float sum(vector<float>&V){
  float sum =0;
  for(int i=0; i<V.size(); i++){
    sum = sum+V[i];
  }
  return sum;
}

int location_of_target(vector<int>&V, int x){
  for(int i=(V.size()-1); i> 0; --i){
    if (x == V[i])
    return i;
  }
  return -1;
}

void shift_right(vector<float>&V){
  float temp = V[V.size()-1];
  for(int i=0; i<=V.size()-1; i++){
    V[i+1]= V[i];
  }

}

int main(){
  /*cout<< sum_from_to(-3,1) << endl;
  cout << enough(9) << endl;
  cout << g_c_d(256,625) << endl;
  cout << gcd2(40,50) << endl;
  cout << isprime(71) << endl;
  int m=25;
  int n=15;
  if (reduce(m,n))
    cout << m << "/" << n <<endl;
  else
    cout << "fraction error" << endl;*/
  /*float a=3.2, b= 5.8, c=0.9;
  /*swap_floats(x,y);
  sort3(a,b,c);
  cout<< a<< " " << b<< " " << c << endl;
  return 0;*/
  /*reverse(V);
  cout << sum(V) << endl;
  for(int i=0; i < V.size(); i++){
    cout<< V[i] << endl;
  }*/
  /*cout << location_of_target(V) << endl;*/
  shift_right(V);
  for(int i=0; i < V.size(); i++){
    cout<< V[i] << endl;
  }
    return 0;
}

