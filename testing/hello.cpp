/* standard "hello world" program. */
#include <iostream>
using std::cout;
using std::cin;
#include <limits.h>

int main()
{
	int x;
	x = INT_MAX;
	int n;

	while(cin >> n){
		if(n < x) {
			x= n;
		}
	}
	cout << "Smallest number was: " << x << "\n";
return 0;
}

// vim:foldlevel=1
